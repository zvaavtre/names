/*

Create a list of fake names based on US census data.

 */

package names

import (
    "crypto/md5"
    "encoding/binary"
    "bytes"
    "fmt"

)


const (
    M = "Male"
    F = "Female"

    FLD_ID = "id"
    FLD_GENDER = "Gender"
    FLD_FIRST = "FirstName"
    FLD_LAST = "LastName"
    FLD_NICK = "NickName"
    FLD_ACCOUNT_NAME = "AccountName"
)



// Generic way to hold info so we can adapt in the future
type Fields map[string]string


// Compute a consistent offset into a range of 0-maxIndex based on the input
// id.  This is fake random in that it hashes the input and uses
// that to generate an offset.
func IndexForLookupFrom(id string, maxIndex uint) uint{
    hash := md5.Sum([]byte(id))
    var bigNumber uint64
    err := binary.Read(bytes.NewReader(hash[:8]),binary.LittleEndian,&bigNumber)
    if err != nil {
        fmt.Println("Error", err)
        return 0
    }
    out := bigNumber % uint64(maxIndex)
    return uint(out)
}

// Helper that will extract a string from the lookup table
// using the IndexForLookupFrom()
func ValueFrom(lookup []string,id string)string{
    return lookup[IndexForLookupFrom(id,uint(len(lookup)))]
}


// Get a page of people.  If you are past the size of the data set
// you'll get an empty slice back
// TODO: Add additonal field provider func that takes the id and returns more fields
func PageOfPeople(page int, pageSize int, sizeOfDataSet int) []Fields{
    if page * (pageSize + 1) > sizeOfDataSet {
        return make([]Fields,0)
    }
    out := make([]Fields,0)
    base := page * pageSize
    for i := base; i < base+pageSize; i++ {
        out = append(out,CreatePerson(fmt.Sprint(i)))
    }
    return out;
}



// Create a set of fields that represent a person
func CreatePerson( id string) Fields{
    f := make(Fields,0)
    f[FLD_ID] = id
    f[FLD_LAST] = ValueFrom(LastNames,id)
    f[FLD_NICK] = ValueFrom(NickNames,id)
    f[FLD_ACCOUNT_NAME] = ValueFrom(AccountNames,id)
    switch IndexForLookupFrom(id,2) {
    case 0:
        f[FLD_GENDER] = M
        f[FLD_FIRST] = ValueFrom(MaleFirsts,id)
    default:
        f[FLD_GENDER] = F
        f[FLD_FIRST] = ValueFrom(FemaleFirstNames,id)
    }
    return f
}










