# Fake Names

Little utility that creates a consistant source of fake names for testing.
 
Optionally you can provide a source of additional data for each name.

KISS - We don't attempt to be fancy with i18n


# Usage

`go get bitbucket.org/zvaavtre/names`


```
import "bitbucket.org/zvaavtre/names"

func SomeSetupFunc(){
    numberOfPeople := 1000000
    
    names.Config(numberOfPeople)

}


func PrintPerson(id int){
    // Will always return the same person for the given id 
    // for a given number of people set at config time
    person := names.PersonAt(id)
    fmt.Printf("First: %s Last: %s",person.First, person.Last)

}

```