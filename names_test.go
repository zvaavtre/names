package names

import (
    "testing"

    "fmt"
)

func TestStuff(t *testing.T){

    fmt.Printf("A person: ", CreatePerson("282"), "\n")

    people := PageOfPeople(4,20,300)

    for i,v := range people{
        fmt.Printf("%d, %v\n",i,v)
    }
}

func TestMoreStuff(t *testing.T){

    v:=IndexForLookupFrom("1", 100)
    fmt.Println("v" , v)

}